import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from "react-router-dom"
import ExampleSSR from './plots/ssr/Example'
import ExampleEffect from './plots/effected/Example'
import Error from './plots/Error'
import './plots.scss'

const router = createBrowserRouter([
  {
    path: "/",
    element: <h2>Home</h2>,
  },
  {
    path: "/example-ssr",
    element: <ExampleSSR />,
  },
  {
    path: "/example-effect",
    element: <ExampleEffect />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
